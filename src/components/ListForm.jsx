import { LibraryBooks } from "@mui/icons-material";
import { Box, Button, InputBase, Typography } from "@mui/material";
import React, { useState } from "react";
import styled from "styled-components";

const ListForm = ({
    handleSubmit,
    heading = false,
    text, setText,
    showButton = false,
    setShowModal = false
}) => {

    const SearchBox = styled(Box)(({theme}) => ({
        display : "flex",
        alignItems :"center",
        border : "1px solid #bdbdbd",
        borderRadius : "5px",
        gap:"5px",
        marginBottom : "5px"
    }))

    return (
        
        
        <form onSubmit={handleSubmit} className='ListForm'>
            <div className='text'>
                <Typography variant="h6" textAlign="center" color="grey">
                    {
                        heading &&
                        <h3>{heading}</h3>
                    }
                </Typography>
                <SearchBox> 
                    <LibraryBooks sx={{color :"white", backgroundColor : "#33eaff", width:"32px", height:"32px", borderRadius:"5px 0 0 5px"}}/>
                    <InputBase placeholder="Todo..." sx={{width:"100%"}} 
                        type="text"
                        value={text} 
                        onChange = { e => setText(e.target.value)}
                        autoFocus
                        name="task"
                    />
                </SearchBox>
            </div>
            {
                showButton && 
                <div className='confirm'>
                    <Button type="submit" valur="submit" sx={{color:"white", width:"100%"}} variant="contained">Confirm</Button>
                </div>
            }
        </form>
    )
}

export default ListForm