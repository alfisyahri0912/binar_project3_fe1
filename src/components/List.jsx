import { Box, Button, Typography } from "@mui/material";
import React from "react";
import Alist from "./Alist";
import Add from "./Add";
import { useState } from "react";


function List(){

    const list = [{
        "id": 1,
        "task": "Nyuci mobil",
        "complete": true
      }, {
        "id": 2,
        "task": "Memberi makan kucing",
        "complete": true
      },

      ]


    return(
        <Box className="List">
            <div className="listTitle">
                <Typography variant="h4" textAlign="center" color="grey" mt={5}>
                    Todo List
                </Typography>
            </div>
            <div className="filterTodolist">
                <Button sx={{color:"white", width:"100%"}} variant="contained">All</Button>
                <Button sx={{color:"white", width:"100%"}} variant="contained">Done</Button>
                <Button sx={{color:"white", width:"100%"}} variant="contained">Todo</Button>
            </div>
            <div className="list">
            {
                list.map( alist => <Alist alist={alist} key={alist.id}/> )
            }
            </div>
            <div className="deleteTodolist">
                <Button color="error" sx={{width:"100%"}} variant="contained">Delete done task</Button>
                <Button color="error" sx={{width:"100%"}} variant="contained">Delete all task</Button>
            </div>
        </Box>

    )
}

export default List