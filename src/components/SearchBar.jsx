import { Search } from "@mui/icons-material";
import { Box, Button, InputBase, styled, Typography } from "@mui/material";
import React from "react";
import Add from "./Add";

function Searchbar(){

    const SearchBox = styled(Box)(({theme}) => ({
        display : "flex",
        alignItems :"center",
        border : "1px solid #bdbdbd",
        borderRadius : "5px",
        gap:"5px",
        marginBottom : "5px"
    }))



    return(
        <Box className="SearchBar">
            <Typography variant="h4" textAlign="center" color="grey" mt={5}>
                Todo Search
            </Typography>
            <div className="TodoSearch">
                <div className="TodoSearchLeft">
                    <SearchBox> 
                        <Search sx={{color :"white", backgroundColor : "#33eaff", width:"32px", height:"32px", borderRadius:"5px 0 0 5px"}}/>
                        <InputBase placeholder="Search..." sx={{width:"100%"}}/>
                    </SearchBox>
                    <Button sx={{color:"white", width:"100%"}} variant="contained">Search</Button>
                </div>
                <div className="TodoSearchRight">
                    <Add/>
                </div>
            </div>
        </Box>
    )
}

export default Searchbar