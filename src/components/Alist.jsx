import { CheckBox, CheckBoxOutlineBlank, Delete} from "@mui/icons-material";
import { Box } from "@mui/system";
import React from "react";
import EditList from "./EditList";

const Alist = ({alist, handleDelete, handleEdit}) => {


    return(
        <Box className="Alist">
            <div className="alist-container"
            >
                <div className='text'>
                    <p style={{color : alist.complete ? 'red' : '#000000', textDecoration : alist.complete ? 'line-through' : ''}}>{alist.task}</p>
                </div>

                <div className='check'>
                    {
                        alist.complete ?
                        <span className='checked'>
                            <CheckBox color='success' />
                        </span>
                        :
                        <span className='unchecked'>
                            <CheckBoxOutlineBlank color=''/>
                        </span>
                    }
                </div>

                <div className='edit'>
                    <span onClick={() => handleEdit(alist.id)}>  
                        <EditList/>
                    </span>
                </div>

                <div className='delete'>
                    {
                        <span onClick={() => handleDelete(alist.id)}>
                            <Delete color="error"/>
                        </span>
                    }
                </div>
            </div>
        </Box>
               
    )
}

export default Alist