import { Box, Button, Modal } from "@mui/material";
import React, { useState } from "react";
import ListForm from "./ListForm";
import { uid } from "uid";
import List from "./List";

const Add = ({}) => {

    const [showModal, setShowModal] = useState(false)
    const [text, setText] = useState('')


    const handleSubmit = (e) => {
        e.preventDefault()
        alert("okee")
    }

    return(
        <Box>
            <Button onClick={e => setShowModal(true)} sx={{color:"white", width:"100%", height:"75px"}} variant="contained">Add Todo List</Button>
            
            <Modal sx={{display:"flex", alignItems:"center", justifyContent:"center"}}
            open={showModal}
            onClose={e => setShowModal(false)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
                <ListForm
                    handleSubmit={handleSubmit}
                    heading="Add New Todo"
                    text={text}
                    setText={setText}
                    showButton={true}
                    setShowModal={setShowModal}
                />
            </Modal>
        </Box>
    )
}

export default Add