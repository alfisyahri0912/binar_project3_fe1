import { Edit } from "@mui/icons-material";
import { Modal } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import ListForm from "./ListForm";

const EditList = () => {

    const [text, setText] = useState()
    const [showModal, setShowModal] = useState(false)

    function handleSubmit(e){

    }

    return(
        <Box>
            <span onClick={e => setShowModal(true)}>
                <Edit sx={{color:"#ff9100"}}/>
            </span>
            <Modal sx={{display:"flex", alignItems:"center", justifyContent:"center"}}
                open={showModal}
                onClose={e => setShowModal(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                >
                    <ListForm
                        handleSubmit={handleSubmit}
                        heading="Edit Todo"
                        text={text}
                        setText={setText}
                        showButton={true}
                        setShowModal={setShowModal}
                    />
            </Modal>
        </Box>

    )
}

export default EditList