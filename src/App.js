import List from './components/List'
import { Box, Container, Stack } from '@mui/material';
import Searchbar from './components/SearchBar';
import './App.css'

function App() {
  return (
      <Container>
        <Stack direction="column">
          <Searchbar/>
          <List/>
        </Stack>
      </Container>
  );
}

export default App;
